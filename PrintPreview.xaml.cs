﻿using MahApps.Metro.Controls;
using System.Windows;

namespace Qed_Mapper {
    /// <summary>
    /// Interaction logic for PrintPreview.xaml
    /// </summary>
    public partial class PrintPreview : MetroWindow {
        string Template;
        public PrintPreview (string template) {
            InitializeComponent();
            Template = template;
        }

        private void Preview_Click (object sender, RoutedEventArgs e) {
            PrintPreviewBrowser.NavigateToString(Template);
        }

        private void Print_Click (object sender, RoutedEventArgs e) {
            mshtml.IHTMLDocument2 doc = PrintPreviewBrowser.Document as mshtml.IHTMLDocument2;
            doc.execCommand("Print", true, null);
        }

        private void Save_Click (object sender, RoutedEventArgs e) {

        }
    }
}
