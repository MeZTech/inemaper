﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Qed_Mapper {
    public static class MapExtensions {

        public static string GeneratePrintable (this Map map, Receiver Receiver, Assembly assembly) {
            // TODO: Please add support for provider and date

            string Template = "";
            using (StreamReader text = new StreamReader(assembly.GetManifestResourceStream("Qed_Mapper.Printable.txt"))) {
                Template = text.ReadToEnd();
            }
            StringBuilder cardBuilder = new StringBuilder();
            // Loop through addresses and get the info
            for (int i = 0; i < Receiver.LoggedMessages.Count; i++) {

                // The original, un-modified address
                string originalAddress = Receiver.LoggedMessages.ToArray()[i];

                // Split it into the main parts
                string[] originalAddressParts = originalAddress.Split(',');

                string streetAddress = originalAddressParts[0].Trim();
                string city = originalAddressParts[1].Trim();

                // Split the originalAddressParts[2] into two parts, state and zip
                string[] stateZipParts = originalAddressParts[2].Trim().Split(' ');

                string state = stateZipParts[0].Trim();
                string zip = stateZipParts[1].Trim();

                // Get the note
                string note = originalAddressParts[4];

                string HumanReadableAddress = streetAddress + ", " + city + ", " + state + " " + zip + ", USA";

                cardBuilder.Append("<div class='card'>");
                cardBuilder.Append("<h1><strong>" + HumanReadableAddress + "</strong> </h1>");
                cardBuilder.Append("<h2>" + note + "</h2>");
                cardBuilder.Append("</div>");
            }

            Template = Template.Replace("@DIRECTIONS", cardBuilder.ToString());
            return Template;
        }

        public static List<Appointment> GetAppointments (this Map map, ComboBox providerBox, ComboBox dateBox) {
            // Keep a list of recognized providers and dates
            List<string> Providers = new List<string>();
            List<string> Dates = new List<string>();

            // Keep a list of appointments so that we can return it
            List<Appointment> AppointmentList = new List<Appointment>();

            // Loop through the appointment entries
            for (int i = 0; i < map.Appointments.Count; i++) {
                DataRow appointment = map.Appointments[i];

                Appointment apptObject = new Appointment();

                // Get the full address
                string fullAddress = appointment["Patient Full Address"].ToString().Trim();

                // Make sure apptObject has all the correct data
                apptObject.RawAddress = fullAddress;

                // Ensure that someone didn't accidentally enter multiple
                // spaces and screw up the program
                fullAddress = Regex.Replace(fullAddress, @"\s+", " ");

                // Remove the comma to find the state part
                fullAddress = fullAddress.Replace(",", "");

                string[] faParts = fullAddress.Split(' ');

                string nstate = "";
                int nstateIndex = 0;

                // Loop through and check to see if the current part is a state abbreviation
                for (int o = 0; o < faParts.Length; o++) {
                    string curPart = faParts[o];
                    if (StateArray.Abbreviations().Contains(curPart) || StateArray.Names().Contains(curPart)) {
                        // The current index is the state
                        nstate = curPart;
                        nstateIndex = o;
                    }
                }

                if (nstateIndex == 0) {
                    throw new Exception("State was not found in address: " + i);
                }

                // The zip code will always come after the state
                string nDate = appointment["Date"].ToString().Trim();
                string ncity = faParts[nstateIndex - 1];
                string nzip = faParts[nstateIndex + 1];
                string nnote = appointment["Patient Misc Value"].ToString().Trim();

                StringBuilder nstreetAddressBuilder = new StringBuilder();
                for (int u = 0; u < nstateIndex - 1; u++) {
                    string curPart = faParts[u];
                    nstreetAddressBuilder.Append(curPart + " ");
                }

                string nstreetAddress = nstreetAddressBuilder.ToString().Trim();
                string nwaypointIdentifier = nstreetAddress.Split(' ')[0] + " " + nzip.Split('-')[0];

                // Update apptObject with all current data
                apptObject.FormattedAddress = nstreetAddress + ", " + ncity + ", " + nstate + " " + nzip;
                apptObject.Patient = appointment["Patient Name"].ToString().Trim();
                apptObject.Provider = appointment["Appointment Provider Name"].ToString().Trim();
                apptObject.FacilityName = appointment["Facility Name"].ToString().Trim();
                apptObject.Date = nDate;
                apptObject.Note = nnote;
                apptObject.WaypointIdentifier = nwaypointIdentifier;

                AppointmentList.Add(apptObject);

                if (!Dates.Contains(nDate)) {
                    // Add the date to the dates list, so we know that we should not add it 
                    // to the combobox again
                    Dates.Add(nDate);

                    // Update the combobox
                    dateBox.Items.Add(nDate);
                }

                if (!Providers.Contains(apptObject.Provider)) {
                    // Add the provider to the providers list, so we know that we should not add it
                    // to the combobox again
                    Providers.Add(apptObject.Provider);

                    // Update the combobox
                    providerBox.Items.Add(apptObject.Provider);
                }
            }

            return AppointmentList;
        }

        /// <summary>
        /// Generate the HTML page that will be used to load the Google Maps map
        /// </summary>
        /// <param name="assembly">Required to get the template document for the page</param>
        /// <param name="appointments">Use the appointments to fill in variables in template</param>
        /// <returns></returns>
        public static string GenerateHTML (Assembly assembly, List<Appointment> appointments) {
            string Template = "";
            using (StreamReader text = new StreamReader(assembly.GetManifestResourceStream("Qed_Mapper.Default.txt"))) {
                Template = text.ReadToEnd();
            }

            // Get the string builders for template fields
            StringBuilder WaypointArray = new StringBuilder();
            StringBuilder WaypointNotesArray = new StringBuilder();

            // Append the beginning for the template fields
            WaypointArray.Append("var waypts = [");
            WaypointNotesArray.Append("var notes = new Array();");

            // Loop through the list of appointments
            for (int i = 0; i < appointments.Count; i++) {
                Appointment appointment = appointments[i];

                if (appointments.Count == 1) {
                    Template = Template.Replace("@ORIGIN", "'" + appointment.FormattedAddress + "'");
                    Template = Template.Replace("@DESTINATION", "'" + appointment.FormattedAddress + "'");
                    WaypointArray.Append("];");
                    WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                } else if (appointments.Count == 2) {
                    if (i == 0) {
                        Template = Template.Replace("@ORIGIN", "'" + appointment.FormattedAddress + "'");
                        WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                        WaypointArray.Append("];");
                    } else {
                        Template = Template.Replace("@DESTINATION", "'" + appointment.FormattedAddress + "'");
                        WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                    }
                } else if (appointments.Count == 3) {
                    if (i == 0) {
                        Template = Template.Replace("@ORIGIN", "'" + appointment.FormattedAddress + "'");
                        WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                    } else if (i == appointments.Count - 1) {
                        Template = Template.Replace("@DESTINATION", "'" + appointment.FormattedAddress + "'");
                        WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                    } else {
                        WaypointArray.Append("{location: '" + appointment.FormattedAddress + "', stopover: true}];");
                        WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                    }
                } else {
                    // The first point must be the origin
                    if (i == 0) {
                        // Replace the origin for the template
                        Template = Template.Replace("@ORIGIN", "'" + appointment.FormattedAddress + "'");
                        WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                    } else if (i == appointments.Count - 1) { // the count - 1 is the last item
                                                              // Replace the destination for the template
                        Template = Template.Replace("@DESTINATION", "'" + appointment.FormattedAddress + "'");
                        WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                    } else if (i == appointments.Count - 2) { // the count - 2 is the last waypoint
                                                              // Make sure that the waypoints and notes array are gucci
                        WaypointArray.Append("{location: '" + appointment.FormattedAddress + "', stopover: true}];");
                        WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                    } else { // The rest are just the normal waypoints
                        WaypointArray.Append("{location: '" + appointment.FormattedAddress + "', stopover: true},");
                        WaypointNotesArray.Append("notes['" + appointment.WaypointIdentifier + "'] = '" + appointment.Note + "';");
                    }
                }
 

            }

            Template = Template.Replace("@WAYPOINTS", WaypointArray.ToString());
            Template = Template.Replace("@NOTES", WaypointNotesArray.ToString());
            Template = Template.Replace("@PROVIDER", "var provider = '" + appointments[0].Provider + "';");
            Template = Template.Replace("@DATE", "var date = '" +appointments[0].Date + "';");

            #region Obsolete (Don't Remove yet)
            /**
            // Loop through the appointment entries
            for (int i = 0; i < map.Appointments.Count; i++) {
                DataRow appointment = map.Appointments[i];

                // Get the data from each appointment
                string streetAddress = appointment["Address"].ToString().Trim();
                string city = appointment["City"].ToString().Trim();
                string state = appointment["State"].ToString().Trim();
                string zip = appointment["Zip"].ToString().Trim();
                string note = appointment["Notes"].ToString().Trim();

                // God help you if you're not in the USA
                string WaypointIdentifier = streetAddress.Split(' ')[0] + " " + zip;

                StringBuilder FormattedAddressBuilder = new StringBuilder();
                string[] streetAddressParts = streetAddress.Split(' ');

                // Loop through the parts of streetAddress and determine how to format them
                for (int k = 0; k < streetAddressParts.Length; k++) {
                    string part = streetAddressParts[k];

                    // Determine numericism
                    int n;
                    bool numeric = int.TryParse(part, out n);

                    if (numeric == true) {
                        // ZIP
                        // Separate ZIP by comma
                        FormattedAddressBuilder.Append(part + ",");
                    } else if (i == streetAddressParts.Length - 1) {
                        // No further parts, comma is not needed
                        FormattedAddressBuilder.Append(part);
                    } else {
                        FormattedAddressBuilder.Append(part + "+");
                    }
                }

                // Append the rest of the address
                FormattedAddressBuilder.Append("," + city + "," + state + "," + zip);
                string formattedAddress = FormattedAddressBuilder.ToString();

                if (i == 0) {
                    // Set the origin and destination since the first 'appointment' is the
                    // provider's address
                    Template = Template.Replace("@ORIGIN", "'" + formattedAddress + "'");
                    Template = Template.Replace("@DESTINATION", "'" + formattedAddress + "'");
                } else if (i == map.Appointments.Count - 2) {
                    // Do this for final waypoint
                    WaypointArray.Append("{location: '" + formattedAddress + "', stopover: true}];");
                    //WaypointNotesArray.Append("'" + note + "',");
                    WaypointNotesArray.Append("notes['" + WaypointIdentifier + "'] = '" + note + "';");
                } else if (i == map.Appointments.Count - 1) {
                    // Do this for final
                    //WaypointNotesArray.Append("'" + note + "'};");
                    WaypointNotesArray.Append("notes['" + WaypointIdentifier + "'] = '" + note + "';");
                } else {
                    // Do this for the waypoints
                    WaypointArray.Append("{location: '" + formattedAddress + "', stopover: true},");
                    WaypointNotesArray.Append("notes['" + WaypointIdentifier + "'] = '" + note + "';");
                    //WaypointNotesArray.Append("'" + note + "',");
                }

            }

            Template = Template.Replace("@WAYPOINTS", WaypointArray.ToString());
            Template = Template.Replace("@NOTES", WaypointNotesArray.ToString());

            map.GeneratedTemplate = Template;
**/
            #endregion
            // DEBUGGING: Console.WriteLine(Template);

            return Template;
            
        }
    }
}
