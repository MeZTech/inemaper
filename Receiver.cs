﻿using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Qed_Mapper {
    /// <summary>
    /// This class will 'receive' data from the HTML Map Page. As the page gets data from
    /// Google Maps, it will call the Log method on this class to send information through
    /// </summary>
    [ComVisible(true)]
    public class Receiver {
        public List<string> LoggedMessages = new List<string>();
        public void Log (string message) {
            LoggedMessages.Add(message);
        }
    }
}
