<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Printable Directions</title>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,900,700italic,900italic);

        * {
            font-family: 'Roboto', sans-serif;
        }

        body {
            background: #D5D5D5;
        }

        h1 {
            font-size: 2rem;
            font-weight: 200;
        }

        strong {
            font-weight: 300;
            color: #539D00;
        }

        h2 {
            font-size: .9rem;
            line-height: 2.5;
            color: gray;
            font-weight: 400;
        }

        .card {
            padding: 1.5rem;
            box-shadow: 0 1px 2px #aaa;
            background: white;
            margin: 0 1rem 1rem;
            border-radius: 3px;
            user-select: none;
            animation: fly-in-from-left .5s 1s ease both;
            transform-origin: top left;
        }
    </style>
</head>
<body>
	@DIRECTIONS
</body>
</html>