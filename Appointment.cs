﻿using System;

namespace Qed_Mapper {
    public class Appointment {

        #region Apointment Attributes
        /// <summary>
        /// These values are the values that must be represented in whatever data set is being used
        /// </summary>
        public string Provider;
        public string Patient;
        public string Date;
        public string FacilityName;
        public string RawAddress;
        public string FormattedAddress;
        public string ReturnAddress;
        public string Note;
        public string WaypointIdentifier;
        #endregion

        /// <summary>
        /// Full Constructor, construct an Appointment with all data at the correct time
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="patient"></param>
        /// <param name="date"></param>
        /// <param name="facilityName"></param>
        /// <param name="rawAddress"></param>
        /// <param name="formattedAddress"></param>
        /// <param name="returnAddress"></param>
        /// <param name="note"></param>
        /// <param name="waypointIdentifier"></param>
        public Appointment(string provider, string patient, string date, string facilityName, string rawAddress, string formattedAddress, string returnAddress, string note, string waypointIdentifier) {
            this.Provider = provider;
            this.Patient = patient;
            this.Date = date;
            this.FacilityName = facilityName;
            this.RawAddress = rawAddress;
            this.FormattedAddress = formattedAddress;
            this.ReturnAddress = returnAddress;
            this.Note = note;
            this.WaypointIdentifier = waypointIdentifier;
        }

        /// <summary>
        /// Blank constructor, initialize values as you go
        /// </summary>
        [Obsolete]
        public Appointment () { }

    }
}
