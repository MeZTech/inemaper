﻿using MahApps.Metro.Controls;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;

namespace Qed_Mapper {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [ComVisible(true)]
    public partial class MainWindow : MetroWindow {
        // Window variables
        string MapTemplate;

        Map MapObject;

        // Replace this receiver
        Receiver Receiver;

        List<Appointment> AllAppointments;
        Dictionary<string, WebBrowser> Browsers;

        // Global Cache
        Cache Cache;

        public MainWindow () {
            //TEST NOT PERMANENT PLACE
            Cache = new Cache();

            InitializeComponent();

            //Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            //AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            this.WindowState = WindowState.Maximized;

            Receiver = new Receiver();

            AllAppointments = new List<Appointment>();
            Browsers = new Dictionary<string, WebBrowser>();

            DateComboBox.SelectionChanged += DateComboBox_SelectionChanged;
            ProviderComboBox.SelectionChanged += ProviderComboBox_SelectionChanged;

            // Get the default template
            // TODO: Add support for templating
            this.MapTemplate = this.LoadTemplate("Default.txt");

            MapBrowser.ObjectForScripting = Receiver;
        }

        int ChangedCount = 0;

        /// <summary>
        /// Update the map page based on the selected healthcare provider
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProviderComboBox_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            if (ChangedCount != 0) {
                UpdateBrowser();
            }
        }

        /// <summary>
        /// Update the map page based on the selected date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DateComboBox_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            UpdateBrowser();
        }

        // Fetches data from comboboxes and dupdates the map page
        private void UpdateBrowser() {
            // Create a string that is a combination of the Provider and Date
            string Provider = (string)ProviderComboBox.SelectedItem;
            string Date = (string)DateComboBox.SelectedItem;
            string identifier = Provider + Date;
            WebBrowser browser = new WebBrowser();

            Assembly assembly = this.GetType().Assembly;
            string source = MapExtensions.GenerateHTML(assembly, GetProviderDateAppointments(Provider, Date));

            this.MapBrowser.NavigateToString(source);

            PrintMenuItem.IsEnabled = true;

            GMapsDM();

            ChangedCount++;
        }

        /// <summary>
        /// Get a list of all appointments a healthcare provider has on a certain date
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        private List<Appointment> GetProviderDateAppointments(string provider, string date) {
            List<Appointment> appointmentList = new List<Appointment>();
            for (int i = 0; i < AllAppointments.Count; i++) {
                Appointment a = AllAppointments[i];

                if(a.Date == date && a.Provider == provider) {
                    appointmentList.Add(a);
                }
            }
            return appointmentList;
        }

        /// <summary>
        /// Get a list of all appointments a provider has
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        private List<Appointment> GetProviderAppointments(string provider) {
            List<Appointment> appointmentList = new List<Appointment>();
            for (int i = 0; i < AllAppointments.Count; i++) {
                Appointment a = AllAppointments[i];

                if (a.Provider == provider) {
                    appointmentList.Add(a);
                }
            }
            return appointmentList;
        }

        #region Extreme Debugging, Do Not Remove
        private void CurrentDomain_UnhandledException (object sender, UnhandledExceptionEventArgs e) {
            throw new NotImplementedException();
        }

        private void Current_DispatcherUnhandledException (object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e) {
            File.WriteAllText("C:\\Users\\silic\\Desktop\\log.txt", e.Exception.Message + " :::: " + e.Exception.InnerException.Message);
        }
        #endregion

        /// <summary>
        /// Allows the user to select and load a worksheet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Open_Click (object sender, RoutedEventArgs e) {

            OpenFileDialog worksheetOpen = new OpenFileDialog();
            worksheetOpen.Filter = "Excel File (*.xlsx)|*.xlsx|Excel 2003 File (*.xls)|*.xls";
            if (worksheetOpen.ShowDialog() == true) {
                DataSet ds = ExcelOps.ReadWorkbook(worksheetOpen.FileName);

                Map m = new Map(ds);
                this.MapObject = m;

                this.AllAppointments = m.GetAppointments(ProviderComboBox, DateComboBox);

                // Populate the comboboxes
                ProviderComboBox.SelectedItem = ProviderComboBox.Items[0];
                DateComboBox.SelectedItem = DateComboBox.Items[0];

            }
        }

        /// <summary>
        /// Used to store certain values returned by GMAPS API
        /// </summary>
        List<AddressCombo> QueriedCombos = new List<AddressCombo>();

        /// <summary>
        /// Google Maps Directions Matrix Method, called to update the browser
        /// </summary>
        private void GMapsDM () {
            string Provider = (string)ProviderComboBox.SelectedItem;

            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.Append("https://maps.googleapis.com/maps/api/distancematrix/json?origins=");

            List<Appointment> ProviderAppointments = GetProviderAppointments(Provider);

            List<string> JSON = new List<string>();
            // At some point in the future I should be punished for writing 
            // such horrible code, but I'm blaming it on being sick for the past 4 days
            // i just want it to work
            if (ProviderAppointments.Count <= 10) {

                StringBuilder odBuilder = new StringBuilder();

                // Loop through all of the provider's appointments
                for (int i = 0; i < ProviderAppointments.Count; i++) {
                    Appointment a = ProviderAppointments[i];

                    string urlAddress = a.FormattedAddress.Replace(",", "");
                    urlAddress = urlAddress.Replace(' ', '+');

                    odBuilder.Append(urlAddress + "|");
                }

                urlBuilder.Append(odBuilder + "&destinations=" + odBuilder);

            } else {
                List<Row> QueriedRows = new List<Row>();

                // So we know when to stop the timer
                int timerTickCount = 0;
                int initialAppointmentCount = ProviderAppointments.Count;
                Timer requestTimer = new Timer();

                // Set the interval so that the requests per hour will
                // never exceed the maximum quota
                requestTimer.Interval = ( 60000 / ProviderAppointments.Count ) + 10;

                // Repeat this until we have enough request URLs to get all the data we need
                requestTimer.Elapsed += (object source, ElapsedEventArgs e) =>
                {
                    StringBuilder newUrlBuilder = new StringBuilder();
                    newUrlBuilder.Append(urlBuilder.ToString());

                    string OriginAddress = FormatAddress(ProviderAppointments[0].FormattedAddress);


                    bool requestAPI = true;
                    // Loop through all the appointments
                    for (int i = 0; i < ProviderAppointments.Count; i++) {

                        Appointment a = ProviderAppointments[i];
                        

                        string urlAddress = FormatAddress(a.FormattedAddress);

                        // Check to see if cache data contains a match
                        var query = from item in Cache.CacheData
                                    where FormatAddress(item.origin) == OriginAddress
                                    where FormatAddress(item.destination) == urlAddress
                                    select item;
                        foreach(AddressCombo ac in Cache.CacheData) {
                            Console.WriteLine("AC: {0}, {1}", FormatAddress(ac.origin), OriginAddress);
                            Console.WriteLine("DC: {0}, {1}", FormatAddress(ac.destination), urlAddress);
                        }

                        Console.WriteLine("FIRST: {0} :: {1}", FormatAddress(Cache.CacheData[0].origin), OriginAddress);
                        if(query.Count() == 1) {
                            requestAPI = false;
                        }

                        // Check to see if it is the first one or not
                        if (i == 0) { // Add it to the origins
                            newUrlBuilder.Append(urlAddress + "&destinations=");
                        } else {
                            newUrlBuilder.Append(urlAddress + "|");
                        }

                    }

                    // Remove the appointment so we're not requesting it again
                    ProviderAppointments.RemoveAt(0);
                    initialAppointmentCount -= 1;
                    
                    // Don't do anything if we have requestAPI set to false
                    if(!requestAPI) {

                    }
                    string URLRequest = newUrlBuilder.ToString();

                    string json = null;

                    // Download the result of the request
                    using (WebClient wc = new WebClient()) {
                        // The result comes in the form of a JSON document
                        json = wc.DownloadString(URLRequest);
                    }

                    // Transform the JSON to actual classes @GoogleMapsData.cs
                    if(json != null) {

                        RootObject ro = JsonConvert.DeserializeObject<RootObject>(json);

                        // More debugging, can't remove until the algorithm is complete
                        //Console.WriteLine("{0}:{1}:{2}", ro.rows[0].elements.Count, ro.destination_addresses.Count, ro.origin_addresses.Count);

                        for (int r = 0; r < ro.rows.Count; r++) {
                            Row row = ro.rows[r];
                            QueriedRows.Add(row);

                            for (int ee = 0; ee < row.elements.Count; ee++) {
                                AddressCombo ac = new AddressCombo();
                                Element element = row.elements[ee];
                                ac.distance = element.distance;
                                ac.duration = element.duration;
                                ac.status = element.status;
                                ac.origin = ro.origin_addresses[0];
                                ac.destination = ro.destination_addresses[ee];
                                QueriedCombos.Add(ac);
                                Cache.Add(ac);


                                //Console.WriteLine("Row {0} Element {1}: {2}:{3}", QueriedRows.Count, ee, element.distance.value, element.duration.value);
                            }
                           
                        }

                    }
                    //Console.WriteLine("T: {0}, I: {1}", timerTickCount, initialAppointmentCount);
                    timerTickCount++;

                    // BROKEN
                    // Stop the timer so we don't send frivolous requests
                    if (timerTickCount == initialAppointmentCount - 2) {
                        requestTimer.Stop();
                    }

                };
                requestTimer.Start();
                // Determine when to stop the timer
                
                // MORE DEBUGGING
                for (int i = 0; i < QueriedCombos.Count; i++) {
                    AddressCombo ac = QueriedCombos[i];
                    if (ac.distance.value / 1000 < 5) {
                        Console.WriteLine("{0}:{1}", ac.origin, ac.destination);
                    }
                }
            }

        }


        private string FormatAddress(string a) {
            string urlAddress = a.Replace(",", "");
            urlAddress = urlAddress.Replace(' ', '+');

            return urlAddress;
        }


        /// <summary>
        /// Returns the distance and the duration together
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public KeyValuePair<int, int>? GetDistDuration(string origin, string destination) {
            for (int i = 0; i < QueriedCombos.Count; i++) {
                if (QueriedCombos[i].origin == origin && QueriedCombos[i].destination == destination) {
                    return new KeyValuePair<int, int>(QueriedCombos[i].distance.value, QueriedCombos[i].duration.value);
                }
            }
            return null;
        }

        /// <summary>
        /// Used to maximize time for large requests while still maintaining a balanced load
        /// </summary>
        private int FindShortestIntervalTime(int addresses) {
            double quotient = 100 / addresses;
            int roundedQuotient = (int) Math.Round(quotient);

            int sum = 0;

            for (int i = addresses; i >= addresses - roundedQuotient; i--) {
                sum += i;
            }

            return 1000 / sum;
        }

        /// <summary>
        /// Load template for the map page
        /// </summary>
        /// <param name="loc"></param>
        /// <returns></returns>
        public string LoadTemplate (string loc) {
            Assembly assembly = this.GetType().Assembly;
            
            foreach(string s in assembly.GetManifestResourceNames()) {
                Console.WriteLine("SS: " + s);
            }
            using (StreamReader text = new StreamReader(assembly.GetManifestResourceStream("Qed_Mapper.Default.txt"))) {
                return text.ReadToEnd();
            }
        }

        /// <summary>
        /// Print a list of the addresses
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintMenuItem_Click (object sender, RoutedEventArgs e) {
            // Check and see if MapObject has been initialized
            if (MapObject == null) {
                MessageBox.Show("You must choose a file first.");
            } else {
                Assembly assembly = this.GetType().Assembly;
                PrintPreview preview = new PrintPreview(MapObject.GeneratePrintable(Receiver, assembly));
                preview.Show();
            }
        }

        /// <summary>
        /// Save the document
        /// TODO: Rewrite this entire method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveAs_Click (object sender, RoutedEventArgs e) {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel File (*.xlsx)|*.xlsx|Excel 2003 File (*.xls)|*.xls";
            if (sfd.ShowDialog() == true) {
                File.WriteAllText(sfd.FileName, MapTemplate);
            }
        }

        /// <summary>
        /// The button that this corresponds to shouldn't work yet
        /// TODO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateSheet_Click (object sender, RoutedEventArgs e) {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel File (*.xlsx)|*.xlsx|Excel 2003 File (*.xls)|*.xls";
            if (sfd.ShowDialog() == true) {
                ExcelOps.CreateWorkbook(sfd.FileName, Receiver);
            }
        }

        protected override void OnClosing (System.ComponentModel.CancelEventArgs e) {
            
            // Save the cache before the program closes
            Cache.Save();

            base.OnClosing(e);
        }
    }
}
