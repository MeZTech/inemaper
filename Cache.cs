﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Qed_Mapper {
    class Cache {

        string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        string DataFolderPath = @"Qed Mapper\";
        string GlCacheFile = "glCache.qhc";

        public List<AddressCombo> CacheData;

        public Cache() {

            // Create the file if it doesn't exist
            string path = Path.Combine(AppDataPath, DataFolderPath);
            if(!File.Exists(path + GlCacheFile)) {
                CreateCacheFile();
                CacheData = new List<AddressCombo>();
            } else {
                // Load the data into memory, and only write it when it's time to close so
                // we can save I/O operations
                CacheData = LoadCacheData();
            }

        }

        public List<AddressCombo> LoadCacheData() {
            string path = Path.Combine(AppDataPath, DataFolderPath);
            string stringData = File.ReadAllText(path + GlCacheFile);
            List<AddressCombo> data = JsonConvert.DeserializeObject<List<AddressCombo>>(stringData);

            return data;
        }
        
        /// <summary>
        /// Create a provider-based cache
        /// </summary>
        /// <param name="ProviderName"></param>
        public void CreateCacheFile(string ProviderName) {

        }

        /// <summary>
        /// Create a complete cache
        /// </summary>
        public void CreateCacheFile() {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string path = Path.Combine(AppDataPath, DataFolderPath);
            if (!Directory.Exists(path)) {
                Directory.CreateDirectory(path);
            }     

            FileInfo cacheFileInfo = new FileInfo(path + GlCacheFile);
            cacheFileInfo.Create();
        }

        public void Add(AddressCombo ac) {
            CacheData.Add(ac);
        }

        public void Save() {
            string saveData = JsonConvert.SerializeObject(CacheData);

            string path = Path.Combine(AppDataPath, DataFolderPath);

            // We need to overwrite the entire file because the CacheData
            // will have all the update information
            File.WriteAllText(path + GlCacheFile, saveData);
        }

    }
}
