﻿using System.Collections.Generic;
using System.Data;

namespace Qed_Mapper {
    public class Map {
        public List<DataRow> Appointments;

        // Strings to control the documents that are generated
        // HTML Document
        public string GeneratedTemplate = "@NULL";

        // Printable Document
        public string GeneratedPrintable = "@NULL";

        private List<DataTable> Worksheets;

        public Map (DataSet ds) {
            Appointments = new List<DataRow>();
            Worksheets = new List<DataTable>();

            // Add all the worksheets from the workbook (ds)
            // Should only be 1 worksheet
            foreach (DataTable dt in ds.Tables) {
                Worksheets.Add(dt);

                // Loop through the appointments and add them to the appt. list
                foreach (DataRow dr in dt.Rows) {
                    Appointments.Add(dr);
                }
            }

        }
    }
}
