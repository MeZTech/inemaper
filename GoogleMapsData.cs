﻿using System.Collections.Generic;

namespace Qed_Mapper {

    public class Distance {
        public string text { get; set; }
        public int value { get; set; }
    }

    public class Duration {
        public string text { get; set; }
        public int value { get; set; }
    }

    public class Element {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
        public string status { get; set; }
    }

    public class Row {
        public List<Element> elements { get; set; }
    }

    public class RootObject {
        public List<string> destination_addresses { get; set; }
        public List<string> origin_addresses { get; set; }
        public List<Row> rows { get; set; }
        public string status { get; set; }
    }

    public class AddressCombo {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
        public string status { get; set; }
        public string origin { get; set; }
        public string destination { get; set; }
    }

}
