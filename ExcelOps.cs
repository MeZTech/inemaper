﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Qed_Mapper {
    public class ExcelOps {

        /// <summary>
        /// Reads the .xlsx workbook from a specified location
        /// </summary>
        /// <param name="location">location</param>
        /// <returns></returns>
        public static DataSet ReadWorkbook (string location) {

            // The dataset is the actual set of appointments
            // We're using a DataSet instead of a table because we eventually want to add multi
            // table support
            DataSet ds = new DataSet();

            // Connect with OleDB
            using (OleDbConnection con = new OleDbConnection(GetConnectionString(location))) {
                con.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;

                // Get all the worksheets
                System.Data.DataTable worksheets = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                // Check to see if there is more than one table
                if (worksheets.Rows.Count > 1) {
                    // TODO: Implement multi-sheet support
                    // TODO: Add alert until multi-sheet support is added
                    // throw new IndexOutOfRangeException("More worksheets than expected. Please verify that workbook is compatible." + " :: " + worksheets.Rows.Count);
                }

                // Get the 'active' worksheet
                foreach (DataRow dr in worksheets.Rows) {
                    string sheetName = dr["TABLE_NAME"].ToString();

                    if (!sheetName.EndsWith("$")) {
                        continue;
                    }

                    // Get all the rows
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

                    // Represent the worksheet as a datatable
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.TableName = sheetName;

                    // Feed data to DataAdapter to adapt and insert into table
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(dt);

                    // Insert table into dataset
                    ds.Tables.Add(dt);
                }
            }

            return ds;
        }

        /// <summary>
        /// Construct a connection string from a simple file location
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        private static string GetConnectionString (string location) {
            // Store all the connection properties
            Dictionary<string, string> props = new Dictionary<string, string>();

            // We should only be handling xlsx files
            // TODO: Add check or compatibilyt for .xls
            props["Provider"] = "Microsoft.ACE.OLEDB.12.0;";
            props["Extended Properties"] = "Excel 12.0 XML";
            props["Data Source"] = location;

            StringBuilder sb = new StringBuilder();

            // Append all the necessary properties
            foreach (KeyValuePair<string, string> prop in props) {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }

            return sb.ToString();
        }


        public static void CreateWorkbook (string location, Receiver Receiver) {
            using (ExcelPackage p = new ExcelPackage()) {

                // The rest of our code will go here...
                p.Workbook.Worksheets.Add("Sheet1");
                ExcelWorksheet ws = p.Workbook.Worksheets[1]; // 1 is the position of the worksheet
                ws.Name = "Sheet1";

                var addressHeader = ws.Cells[1, 1];
                addressHeader.Value = "Address";
                addressHeader.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 15));

                var cityHeader = ws.Cells[1, 2];
                cityHeader.Value = "City";
                cityHeader.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 15));

                var stateHeader = ws.Cells[1, 3];
                stateHeader.Value = "State";
                stateHeader.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 15));

                var zipHeader = ws.Cells[1, 4];
                zipHeader.Value = "Zip";
                zipHeader.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 15));

                var noteHeader = ws.Cells[1, 5];
                noteHeader.Value = "Note";
                noteHeader.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 15));

                for (int i = 0; i < Receiver.LoggedMessages.Count; i++) {

                    // The original, un-modified address
                    string originalAddress = Receiver.LoggedMessages.ToArray()[i];

                    // Split it into the main parts
                    string[] originalAddressParts = originalAddress.Split(',');

                    string streetAddress = originalAddressParts[0].Trim();
                    string city = originalAddressParts[1].Trim();

                    // Split the originalAddressParts[2] into two parts, state and zip
                    string[] stateZipParts = originalAddressParts[2].Trim().Split(' ');

                    string state = stateZipParts[0].Trim();
                    string zip = stateZipParts[1].Trim();

                    // Get the note
                    string note = originalAddressParts[4];

                    // Get the cells and set the values
                    var addressCell = ws.Cells[i + 2, 1];
                    addressCell.Value = streetAddress;

                    var cityCell = ws.Cells[i + 2, 2];
                    cityCell.Value = city;

                    var stateCell = ws.Cells[i + 2, 3];
                    stateCell.Value = state;

                    var zipCell = ws.Cells[i + 2, 4];
                    zipCell.Value = zip;

                    var noteCell = ws.Cells[i + 2, 5];
                    noteCell.Value = note;

                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                // Save the Excel file
                Byte[] bin = p.GetAsByteArray();
                File.WriteAllBytes(location, bin);


            }
        }
    }
}
